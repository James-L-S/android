package entities

data class FlashcardSet(val title: String)

fun main(args: List<FlashcardSet>){
    val flashcardSet1 = FlashcardSet("Biology")
    val flashcardSet2 = FlashcardSet("Chemistry")
    val flashcardSet3 = FlashcardSet("Data Science")
    val flashcardSet4 = FlashcardSet("English")
    val flashcardSet5 = FlashcardSet("Kotlin")
    val flashcardSet6 = FlashcardSet("Java")
    val flashcardSet7 = FlashcardSet("History")
    val flashcardSet8 = FlashcardSet("Game Theory")
    val flashcardSet9 = FlashcardSet("Math")
    val flashcardSet10 = FlashcardSet("Health")
// Was not sure on how the list is callable to print from it.
    println(flashcardSet1)
    println(flashcardSet2)
    println(flashcardSet3)
    println(flashcardSet4)
    println(flashcardSet5)
    println(flashcardSet6)
    println(flashcardSet7)
    println(flashcardSet8)
    println(flashcardSet9)
    println(flashcardSet10)

}
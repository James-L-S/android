package entities

data class Flashcard(val term: String, val answer: Boolean)

fun main(args: List<Flashcard>){
    val flashcard1 = Flashcard("Is this due on Wednesday?", true)
    val flashcard2 = Flashcard("Is this due on Monday?", false)
    val flashcard3 = Flashcard("Is this due on Tuesday?", false)
    val flashcard4 = Flashcard("Is this due on Friday?", false)
    val flashcard5 = Flashcard("This is going to work on IOS?", false)
    val flashcard6 = Flashcard("We should follow CDC guidelines and wear masks?", true)
    val flashcard7 = Flashcard("The answer is true, Im running out of ideas.", true)
    val flashcard8 = Flashcard("Is this due on Saturday?", false)
    val flashcard9 = Flashcard("It has been a while since Ive used object oriented programming?", true)
    val flashcard10 = Flashcard("This is the last flashcard?", true)

    println(flashcard1)
    println(flashcard2)
    println(flashcard3)
    println(flashcard4)
    println(flashcard5)
    println(flashcard6)
    println(flashcard7)
    println(flashcard8)
    println(flashcard9)
    println(flashcard10)
}




